---
theme: gaia
_class: lead
paginate: false
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

![center](https://www.atlassian.com/pl/dam/jcr:e33efd9e-e0b8-4d61-a24d-68a48ef99ed5/Jira%20Software@2x-blue.png)

---

# Czym jest Jira?

### Jira to narzędzie do zarządzania projektami oraz śledzenia przebiegu problemów.
### Pozwala na podzielenie dużego, skomplikowanego projektu na mniejsze, prostsze do zarządzania komponenty.

---

# Zalety

- Bardzo elastyczna (nadaje się do projektów o różnym rozmiarze lub domenie)
- Skalowalna
- Pozwala naturalnie dzielić zadania na dużo mniejsze
- ...

---

# Wady

- Na początku może wydawać się dość skomplikowana
- Utrzymywanie projektu w aktualnym stanie wymaga sporej dyscypliny w zespole

---

# Najważniejsze funkcjonalności

- Zarządzanie czasem (estymacje, logowanie, raportowanie)
- Śledzenie postępów w projekcie
- Dzielenie dużych zadań na znacznie mniejsze (Epic -> Story -> Task)
- Integracja z zewnętrznymi serwisami (Serwisy Git, Slack, Teams, ...)
- Raportowanie

---
# Prezentację wykonał:
### Piotr Kupczyk
### s22001